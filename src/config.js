var version = "1.0.0"
var url = "https://ylstudio.com.mx/api/" //prod
// var url = "http://localhost:8000/api/" // des
export default {
    url() {
        return url
    },
    version() {
        return version
    },
}