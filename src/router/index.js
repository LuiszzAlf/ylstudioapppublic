import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Principal from '../components/Principal.vue'
import Citas from '../components/Citas.vue'
import Cita from '../components/Cita.vue'
import Qr from '../components/Qr.vue'
import Clientes from '../components/Clientes.vue'
import Codigos from '../components/Codigos.vue'
import Horarios from '../components/Horarios.vue'
import Horariosvencidos from '../components/HorariosVencidos.vue'
import Promos from '../components/Promos.vue'
import Ubicaciones from '../components/Ubicaciones.vue'
import Servicios from '../components/Servicios.vue'
import Usuarios from '../components/Usuarios.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/principal',
        name: 'Principal',
        component: Principal,
    },
    {
        path: '/citas',
        name: 'Citas',
        component: Citas,
    },
    {
        path: '/citas_scan',
        name: 'Escanear Citas',
        component: Qr,
    },
    {
        path: '/clientes',
        name: 'Clientes',
        component: Clientes,
    },
    {
        path: '/codigos',
        name: 'Codigos',
        component: Codigos,
    },
    {
        path: '/horarios',
        name: 'Horarios',
        component: Horarios,
    },
    {
        path: '/horariosvencidos',
        name: 'Horariosvencidos',
        component: Horariosvencidos,
    },
    {
        path: '/promos',
        name: 'Promos',
        component: Promos,
    },
    {
        path: '/servicios',
        name: 'Servicios',
        component: Servicios,
    },
    {
        path: '/ubicaciones',
        name: 'Ubicaciones',
        component: Ubicaciones,
    },
    {
        path: '/usuarios',
        name: 'Usuarios',
        component: Usuarios,
    },
    {
        path: '/about',
        name: 'About',
        component: About,
    },
    {
        path: '/cita/:folio',
        name: 'Cita',
        component: Cita,
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router