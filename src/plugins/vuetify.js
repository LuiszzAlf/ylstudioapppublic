import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from "./lang"

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        theme: { dark: true },
        themes: {
            light: {
                primary: '#2b81d6',
                headers: '#6c2526',
                secondary: '#424242',
                accent: '#82B1FF',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107'
            },
        },
    },

    lang: {
        locales: {
            es
        },
        current: 'es',
    },

});