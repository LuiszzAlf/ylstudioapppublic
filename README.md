# ylstudioapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
https://medium.com/@fthialem/vuejs-and-vuetify-for-android-app-powered-by-capacitor-bbb99454e4af
